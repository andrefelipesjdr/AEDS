//Andre Felipe Jose Santos 162050077
/*Escreva uma programa que leia do usuário os nomes de dois arquivos texto. Em
seguida, o programa deve criar um terceiro arquivo texto com o conteúdo dos dois primeiros juntos
(o conteúdo do primeiro arquivo seguido do conteúdo do segundo).
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void){
FILE *p1, *p2, *p3;
char str1[10],str2[10],nome[10];

	printf("Digite o nome do primeiro arquivo: ");
	gets(nome);
	strcat(nome,".txt");
	p1=fopen(nome, "a+");
	fgets(str1,10,p1);
	fclose(p1);

	printf("Digite o nome do segundo arquivo: ");
	gets(nome);
	strcat(nome,".txt");
	p2=fopen(nome, "a+");
	
	fgets(str2,10,p2);
	fclose(p2);

	strcat(str1,str2);

	p3=fopen("uniao.txt", "a+");
	fputs(str1,p3);

	}
