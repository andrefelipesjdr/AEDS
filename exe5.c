//Andre Felipe Jose Santos 162050077
/*Faça um programa que leia números positivos e os converta em binário. Cada número
binário deverá ser salvo em uma linha de um arquivo texto. O programa termina quando o usuário
digitar um número negativo
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void){
FILE *p;
int num,resul,i; 
 label:
 printf("Digite o numero: ");
 scanf("%d", &num);
 if(num>0){
	p=fopen("binario.txt","a+");
		for(i=31;i>=0;i--){
			resul=num>>i;
		
		if(resul & 1){
			fprintf(p,"%d",1);
		} 
		else{
			fprintf(p,"%d",0);
		}
	}
	fputs("\n",p);
	goto label;
	}
	else{
		fclose(p);
		return 0;
		}
}
