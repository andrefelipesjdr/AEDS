//Andre Felipe Jose Santos 162050077
/*Faça um programa que leia um arquivo contendo uma lista de compras. Cada linha
do arquivo possui um nome, quantidade e valor unitário do produto. O programa então exibe o total
da compra.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(void){
FILE *p1;
char str[100];
float q,v, t=0.0;

p1=fopen("compras.txt", "r");

while(!feof(p1)){
	fscanf(p1,"%s %f %f", str,&q,&v);
	
	t+= q*v;
	}
printf("%.2f ", t);
fclose(p1);
}
